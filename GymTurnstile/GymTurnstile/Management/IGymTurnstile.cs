﻿using GymTurnstile.Model;
using GymTurnstile.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymTurnstile.Management
{
    public interface IGymTurnstile
    {
        public void Add(UserDTO userDTO);
        public void UpdateOut(IGymObservable gymObservable, int? taskId);
        public void UpdateEnter(IGymObservable gymObservable);
        public ObservableCollection<Message> getLog();
        public void SettLog(ObservableCollection<Message> Log);
        int countT();
    }
}
