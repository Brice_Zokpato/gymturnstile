﻿using GymTurnstile.Model;
using GymTurnstile.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace GymTurnstile.Management
{
    public class GymTurnstileManager : IGymTurnstile
    {
        #region Fields
            #region Static Fields
        private static readonly GymTurnstileManager instance = new GymTurnstileManager();
        private static readonly object padlock = new object();
        private static readonly int _TURNSTILE_MAX_SIZE = 10;
        private static int _TURNSTILE_SIZE = 0;
        #endregion Static Fields
        #region Internal Fields
        private readonly List<Task> Turnstile = new List<Task>();
        private ObservableCollection<Message> _logs = new ObservableCollection<Message>();
        private readonly ConcurrentQueue<UserDTO> TurnstileQueue = new ConcurrentQueue<UserDTO>();
        #endregion Internal Fields
        #endregion  Fields
        #region Constructor
        private GymTurnstileManager() { }
        #endregion Constructor
        #region Singleton Pattern
        public static GymTurnstileManager Instance { get { lock(padlock) { return instance; } } }


        #endregion Singleton Pattern
        #region Methods
        public void UpdateOut(IGymObservable gymObservable, int? taskId)
        {
            lock (padlock) 
            {
                if (taskId != null)
                {
                    var task = Turnstile.FirstOrDefault(task => task.Id == taskId);
                    if (task != default && Turnstile.Remove(task))
                    {
                        //task.
                        _TURNSTILE_SIZE--;
                        if (TurnstileQueue.TryDequeue(out var userDTO))
                        {
                            Add(userDTO);
                            App.Current.Dispatcher.BeginInvoke(() =>
                            {
                                _logs.Add(Message.getMessageByType(Message.MessageType.Exit, userDTO));
                            });
                        }
                        
                       
                    }
                }

            }

        }

        public void Add(UserDTO userDTO)
        {
            lock (padlock)
            {
                if (_TURNSTILE_SIZE >= _TURNSTILE_MAX_SIZE)
                {
                    TurnstileQueue.Enqueue(userDTO);
                    App.Current.Dispatcher.BeginInvoke(() =>
                    {
                        _logs.Add(Message.getMessageByType(Message.MessageType.EnterQueue, userDTO));
                    });
                        
                }
                else
                {
                    Turnstile.Add(User.getUserTask(userDTO.FirstName, userDTO?.LastName, this));
                    _TURNSTILE_SIZE++;
                    App.Current.Dispatcher.BeginInvoke(() =>
                    {
                        _logs.Add(Message.getMessageByType(Message.MessageType.Enter, userDTO));
                    });
                    
                }
            }
            
            
        }

        public int countT() => Turnstile.Count;

        private string notifyEnterMessage(IGymObservable gymObservable) => $"User {gymObservable} Entered the room !";


        public void UpdateEnter(IGymObservable gymObservable)
        {
            Debug.WriteLine(notifyEnterMessage(gymObservable));
        }

        public ObservableCollection<Message> getLog()
        {
            return _logs;
        }

        public void SettLog(ObservableCollection<Message> Log)
        {
            _logs = Log;
        }
        #endregion Methods

    }
}
