﻿using GymTurnstile.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymTurnstile.Model
{
    public class Message
    {
        public string? text { get; set; }
        public DateTime? timestamp { get; set; }

        private Message() { }

        public static Message getMessageByType(MessageType messageType, UserDTO userDTO = default)
        {
            var message = new Message();
            message.timestamp = DateTime.UtcNow;
            switch (messageType)
            {
                case MessageType.Enter: message.text = $"User {userDTO} Entered in Gym Turnstile...";
                    break;
                case MessageType.EnterQueue:
                    message.text = $"User {userDTO}  Entered in Queue...";
                    break;
                case MessageType.Exit:
                default:
                    message.text = $"User {userDTO} Exit !";
                    break;
            }

            return message;
        }

        public enum MessageType
        {
            Enter,
            EnterQueue,
            Exit
        }
    }
}
