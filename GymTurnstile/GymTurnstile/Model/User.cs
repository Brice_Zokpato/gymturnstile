﻿using GymTurnstile.Management;
using GymTurnstile.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace GymTurnstile.Model
{
    public class User : IGymObservable
    {
        private string? FirstName { get; set; }
        private string? LastName { get; set; }
        public static int cpt = 0;
        public static Task getUserTask(string firstname, string lastname, IGymTurnstile observer)
        {   
            return Task.Run(() =>
            {
                cpt++;
                Debug.WriteLine($"{firstname}  :  {cpt}");
                var user = new User() { FirstName = firstname, LastName = lastname };
                var tid = Task.CurrentId;
                var t = new System.Timers.Timer();
                t.Interval = 10000;
                t.Elapsed += (sender, e) => user.notify(observer, tid); 
                t.Enabled = true;
                t.Start();

            });
        }

       

        public void notify(IGymTurnstile gymObserver, int? taskId)
        {
            gymObserver.UpdateOut(this, taskId);
        }

        public void notifyEnter(IGymTurnstile gymObserver)
        {
            gymObserver.UpdateEnter(this);
        }

        public override string ToString()
        {
            return $"{FirstName}  {LastName}";
        }

    }

    public class UserDTO
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }

        public override string ToString()
        {
            return $"{FirstName}  {LastName}";
        }
    }
}
