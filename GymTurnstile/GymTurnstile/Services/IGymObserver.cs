﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymTurnstile.Services
{
    public interface IGymObserver
    {
        public void UpdateOut(IGymObservable gymObservable, int? taskId);
        public void UpdateEnter(IGymObservable gymObservable);
    }
}
