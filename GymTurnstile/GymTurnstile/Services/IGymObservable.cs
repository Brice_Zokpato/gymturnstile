﻿using GymTurnstile.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymTurnstile.Services
{
    public interface IGymObservable
    {
        public void notify(IGymTurnstile gymObserver, int? taskId);
        public void notifyEnter(IGymTurnstile gymObserver);
    }
}
